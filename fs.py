
from abc import ABCMeta, abstractmethod

from configparser import ConfigParser

class Account(object):
    def __init__(self, accid, name, value=None):
        self._accid = accid
        self._name = name
        self._value = value
    
    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        self._name = name

    @property
    def value(self):
        return self._value

    @value.setter
    def value(self, value):
        self._value = value

class FinStatement(object):
#class FinStatement(object, metaclass=ABCMeta):
    def __init__(self, period=None):
        self._period = period
        self._accounts = {}

    @property
    def accounts(self):
        return self._accounts

    @accounts.setter
    def accounts(self, accounts):
        self._accounts = accounts

    def show(self):
        i = 0
        for item in self._accounts:
            if i>3: break 
            print('account: {}, value: {}'.format(item, self._accounts[item].name))
            i+=1


'''
class BalanceSheet(FinStatement):

class IncomeStatement(FinStatement):
    #

class AnalysisPanel(object):


class StrategicFS()
'''

