
from enum import Enum
from configparser import ConfigParser
import logging
import sys, getopt

from openpyxl import load_workbook, Workbook
from openpyxl.styles import Font

class PYXL_Inf(object):
    MAX_FS_ROWS = 100
    
    def __init__(self):
        self._db = [[0 for x in range(52) ] for y in range(12)]


    def readin(self, src_file_path, sheet):
        template_wb = load_workbook(src_file_path, read_only=True)
        ws = template_wb[sheet]
        for i in range(12):
            for j in range(52):
                self._db[i][j]=ws.cell(row=121+i, column=6+j).value

    def writeout(self, dest_file_path, sheet):
        template_wb = load_workbook(dest_file_path)
        ws = template_wb[sheet]
        for i in range(10):
            for j in range(3): #month as column
                if i<=8:
                    ws.cell(row=103+i, column=38+j).value = self._db[i][35+j]
                elif i==9: #counts of students
                    ws.cell(row=103+i, column=38+j).value = self._db[i+2][35+j]
        for i in range(10):
            for j in range(5): #month as column
                if i<=8:
                    ws.cell(row=103+i, column=41+j).value = self._db[i][39+j]
                elif i==9:
                    ws.cell(row=103+i, column=41+j).value = self._db[i+2][39+j]
        template_wb.save('金芭蕾预测20200720.xlsx')

def main():
    logging.basicConfig(filename = 'pyxl.log', level = logging.DEBUG, \
            filemode='w', format='%(message)s')
    #TODO: how to duplicate python objects?
    pyxl_inf = PYXL_Inf()
    pyxl_inf.readin('单店月度运营数据 2020.05.xlsx', '上海')
    pyxl_inf.writeout('金芭蕾预测20200720.xlsx', '上海门店历史统计')
    
'''
    argv = sys.argv[1:]
    try:
        opts, args = getopt.getopt(argv, 'i:o')
    except getopt.GetoptError:
        print('-i <infile> -o <outfile>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-i':
            orig_filename = arg
        elif opt == '-o':
            out_filename= arg
        else:
            print('Invalid argument.')
'''

if __name__=='__main__':
    main()

