
from enum import Enum
from configparser import ConfigParser
import logging
import sys, getopt

from openpyxl import load_workbook, Workbook

from fs import Account, FinStatement

class In_FS_Formats(Enum):
    EastMoney = 0

Tmplt_Col_Acc_Name = 1
Tmplt_Col_Acc_Id = 2

Cash=''
AR=''
PIA=''
OR=''


class PYXL_Inf(object):
    MAX_FS_ROWS = 83
    
    def __init__(self, start_row=2):
        self._acc_start_row = start_row
        self._actual_rows = None

    def format_clean(self, format_type, in_filename, out_filename):
        if format_type == In_FS_Formats.EastMoney:
            self.format_clean_em(in_filename, out_filename)
        else:
            logging.debug('Unsupported file format.')

    def format_clean_em(self, in_filename, out_filename):
        PYXL_Inf.file_copy(in_filename, out_filename)

        out_wb = load_workbook(filename=out_filename)
        out_array = []
        out_ws = out_wb.active

        for r in range(self._acc_start_row, PYXL_Inf.MAX_FS_ROWS+1):
            acc_name = out_ws.cell(row=r, column=1).value
            if acc_name :
                out_ws.cell(row = r, column=1).value = acc_name.split('(')[0]
            #out_array.append(acc_name)
        out_wb.save(out_filename)

    def file_copy(src_file_path, dest_file_path):
        try: 
            with open(src_file_path, "rb") as src_file:
                src_file_bytes=src_file.read()
        except FileNotFoundError:
            print("File does not exist.")
        try:
            with open(dest_file_path, "wb") as dest_file:
                dest_file.write(src_file_bytes)
        except IOError:
            print("IO error.")


    def readin_template(self, fs, template_filename):
        try: 
            template_wb = load_workbook(template_filename, read_only=True)
        except FileNotFoundError:
            print("File does not exist.")
            return False

        ws = template_wb['item id']
        #ws = template_wb.active
        #logging.debug('Reading sheet: %s' % (ws.Name))
        accounts = fs.accounts
        for row in range(self._acc_start_row, PYXL_Inf.MAX_FS_ROWS):
            name = ws.cell(row=row, column=Tmplt_Col_Acc_Name).value
            if not name: continue
            if '其中' in name: 
                true_name = name.split('：')[-1]
                if not true_name:
                    name = name.split(':')[-1]
                else:
                    name = true_name
            accid = ws.cell(row=row, column=Tmplt_Col_Acc_Id).value
            logging.debug('Found account: %s' % (name))
            account = Account(accid, name)
            accounts[name]=account
            #accounts.append(account)
        fs.show()

    def dump_xlsx(self, fs, out_filename):
        out_wb = Workbook()
        ws = out_wb.active
        for r, account in enumerate(fs.accounts):
            ws.cell(row = r+1, column = 1).value = account.name
            logging.debug('writing value: %s' % (account.name))
        out_wb.save(out_filename)

def main():
    logging.basicConfig(filename = 'pyxl.log', level = logging.DEBUG, \
            filemode='w', format='%(message)s')
    fs = FinStatement()
    #TODO: how to duplicate python objects?
    pyxl_inf = PYXL_Inf()
    argv = sys.argv[1:]
    try:
        opts, args = getopt.getopt(argv, 'f:t:')
    except getopt.GetoptError:
        print('-f <infile> -t <outfile>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-f':
            orig_filename = arg
            out_filename=''
            strings = orig_filename.split('.')
            for i in range(0, len(strings)-1):
                out_filename += strings[i]
            #the cleaned the file has a file name with keyword 'clean'
            out_filename += '_clean'+'.'+strings[-1]
            pyxl_inf.format_clean(In_FS_Formats.EastMoney, \
                    orig_filename, out_filename)
        elif opt == '-t':
            pyxl_inf.readin_template(fs, 'fs_template.xlsx')
            #pyxl_inf.dump_xlsx(fs, arg)
        else:
            print('Invalid argument.')

if __name__=='__main__':
    main()

